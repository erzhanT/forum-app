const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const Comment = require('../models/Comment');

router.get('/', async (req, res) => {
    const comments = await Comment.find({post: req.query.post}).sort({"datetime": -1}).populate({path: "user"});
    if (comments) {
        res.send(comments);
    } else {
        res.sendStatus(404);
    }
});

router.post('/', auth, async (req, res) => {
    const commentData = req.body;
    commentData.user = req.user._id;
    commentData.datetime = new Date();
    const comment = new Comment(commentData)
    try {
        await comment.save();
        res.send(comment);
    } catch (e) {
        res.status(400).send(e);
    }
});

module.exports = router;