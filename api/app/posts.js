const path = require("path");
const express = require('express');
const config = require("../config");
const Post = require('../models/Post');
const auth = require('../middleware/auth');
const multer = require('multer');
const {nanoid} = require('nanoid');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const upload = multer({storage});


router.get('/', async (req, res) => {

    const posts = await Post.find().sort({"datetime": -1});
    if (posts) {
        res.send(posts);
    } else {
        res.sendStatus(404);
    }
});

router.get('/:id', async (req, res) => {

    const post = await Post.findById(req.params.id).populate({path: "user"});
    if (post) {
        res.send(post);
    } else {
        res.sendStatus(404);
    }
});

router.post('/', auth, upload.single("image"), async (req, res) => {
    const postData = req.body;
    if (req.file) {
        postData.image = req.file.filename;
    }
    // if (req.file) {
    //     postData.image = 'uploads' + req.file.filename;
    // }
    postData.user = req.user._id;
    postData.datetime = new Date();
    const post = new Post(postData)
    try {
        if ((post.description && !post.image) || (!post.description && post.image)) {
            await post.save();
            res.send(post);
        } else {
            res.status(400).send({message: "Fill in one field"});        }
    } catch (e) {
        res.status(400).send(e);
    }
});

module.exports = router;