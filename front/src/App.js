import React from 'react';
import Layout from "./components/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Posts from "./containers/Posts/Posts";
import CurrentPost from "./containers/CurrentPost/CurrentPost";
import Login from "./containers/Login/Login";
import PostForm from "./containers/Posts/PostForm";

function App() {
  return (
      <div className="App">
        <Layout/>
        <Switch>
          <Route path="/" exact component={Posts}/>
          <Route path="/posts/:id" exact component={CurrentPost}/>
          <Route path="/signup" exact component={Register}/>
          <Route path="/signin" exact component={Login}/>
          <Route path="/addpost" exact component={PostForm}/>
          <Route render={() => <h1>404</h1>}/>
        </Switch>
      </div>
  );
}

export default App;