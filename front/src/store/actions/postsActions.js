import axiosAPI from "../../axiosApi";
import {historyPush} from "./historyActions";

export const FETCH_POSTS_SUCCESS = "FETCH_POSTS_SUCCESS";
export const FETCH_POSTS_ERROR = "FETCH_POSTS_ERROR";

export const ADD_POST_SUCCESS = "ADD_POST_SUCCESS";
export const ADD_POST_ERROR = "ADD_POST_ERROR";

export const FETCH_POST_SUCCESS = "FETCH_POST_SUCCESS";
export const FETCH_POST_ERROR = "FETCH_POST_ERROR";

export const FETCH_COMMENTS_SUCCESS = "FETCH_COMMENTS_SUCCESS";
export const FETCH_COMMENTS_ERROR = "FETCH_COMMENTS_ERROR";

export const ADD_COMMENT_SUCCESS = "ADD_COMMENT_SUCCESS";

const fetchPostsSuccess = value =>  ({type: FETCH_POSTS_SUCCESS, value});
const fetchPostsError = error => ({type: FETCH_POSTS_ERROR, error});

const fetchPostSuccess = value => ({type: FETCH_POST_SUCCESS, value});
const fetchPostError = error => ({type: FETCH_POST_ERROR, error});

const addPostError = error => ({type: ADD_POST_ERROR, error});
const addPostSuccess = value => ({type: ADD_POST_SUCCESS, value});

const fetchCommentsSuccess = value => ({type: FETCH_COMMENTS_SUCCESS, value});
const fetchCommentsError = error => ({type: FETCH_COMMENTS_ERROR, error});

const addCommentSuccess = (value) => ({type: ADD_COMMENT_SUCCESS, value});

export const fetchPosts = () => {
    return async dispatch => {
        try {
            const response = await axiosAPI.get("/posts");
            dispatch(fetchPostsSuccess(response.data));
        } catch (e) {
            dispatch(fetchPostsError(e));
        }
    };
};



export const addPost = (data) => {
    return async (dispatch, getState) => {
        const headers = {
            "Authorization": getState().users.user && getState().users.user.user.token
        };
        try {
            const response = await axiosAPI.post('/posts', data, {headers});
            dispatch(addPostSuccess(response.data));
            dispatch(historyPush('/'))
        } catch (e) {
            dispatch(addPostError());
        }
    };
};



export const fetchPost = (id) => {
    return async dispatch => {
        try {
            const response = await axiosAPI.get("/posts/" + id);
            dispatch(fetchPostSuccess(response.data));
        } catch (e) {
            dispatch(fetchPostError(e));
        }
    };
};



export const fetchComments = (id) => {
    return async dispatch => {
        try {
            const response = await axiosAPI.get("/comments?post=" + id);
            dispatch(fetchCommentsSuccess(response.data));
        } catch (e) {
            dispatch(fetchCommentsError(e));
        }
    };
};



export const addComment = (data) => {
    return async (dispatch, getState) => {
        const headers = {
            "Authorization": getState().users.user && getState().users.user.user.token
        };
        try {
            const response = await axiosAPI.post('/comments', data, {headers});
            dispatch(addCommentSuccess(response.data));
        } catch (e) {
            dispatch(addPostError(e));
        }
    };
};